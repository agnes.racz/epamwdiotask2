export default class PasteTableComponent {

    get rootEl() {
        return $(".highlighted-code");
    }

    get syntaxValue() {
        return this.rootEl.$(".left .btn.h_800");
    }

    get bashCode() {
        return this.rootEl.$(".source");
    }


}