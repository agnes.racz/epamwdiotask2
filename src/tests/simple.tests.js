import Basepage from "../po/pages/base.page.js";
import CreatedPastePage from "../po/pages/createdPaste.page.js";

const basepage = new Basepage();
const createdPastePage = new CreatedPastePage();

const code = `git config --global user.name  'New Sheriff in Town'
            git reset $(git commit - tree HEAD ^ { tree } - m 'Legacy code')
            git push origin master--force`;

describe('Paste page', () => {

    beforeEach(async () => {
        await basepage.open();
    })

    it('Check if paste was created in conformity with input', async () => {

        await $(".css-47sehv").click(); //clicking the Agree button on the "We value your privacy" modal

        await basepage.form.input("code").setValue(code);
        await basepage.form.input("syntax").click();
        await basepage.form.syntaxValue("bash").click();
        await basepage.form.input("expiration").click();
        await basepage.form.expirationValue("tenMinutes").click();
        await basepage.form.input("title").setValue("how to gain dominance among developers");
        await basepage.form.createPasteBtn.click();

        await expect(browser).toHaveTitle("how to gain dominance among developers - Pastebin.com");
        const syntax = await createdPastePage.pasteTable.syntaxValue;
        await expect(syntax).toHaveText("Bash");
        const bashCode = await createdPastePage.pasteTable.bashCode;
        await expect(bashCode).toHaveText(code);

    })

})