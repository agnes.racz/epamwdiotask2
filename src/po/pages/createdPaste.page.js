import PasteTableComponent from "../components/pasteTable.component.js";

export default class CreatedPastePage {

    constructor() {
        this.pasteTable = new PasteTableComponent();
    }

}