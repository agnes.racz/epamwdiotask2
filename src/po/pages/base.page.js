import FormComponent from "../components/form.component.js";

export default class Basepage {

    constructor() {
        this.form = new FormComponent();
    }

    open() {
        return browser.url("https://pastebin.com/");
    }
}